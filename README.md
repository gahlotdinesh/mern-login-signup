## MERN Login Signup Component

Minimalistic, ready-to-use component for Sessions based Login and Sign-Up using Reactjs, Redux, Bootstrap Node.js + Express and MongoDB.

## Features

- Login page with success/error messages
- Register page with success/error messages
- Protected Profile page route that needs authentication to access
- Persistence achieved using Sessions, with session ID stored on Cookie
- Logout deletes session in database and cookie from browser



## Prerequisites

- Node.js
- NPM
- React
- MongoDB Atlas MongoURI
- **.env file with ENV variables**, a .env template is provided

## Quick Start


```

Install packages for Node backend

```
 cd mern-login-signup
 npm install
```

Install packages for React client

```
 cd mern-login-signup/client
 npm install
```
`


Start Dev Server ( both React server and Nodejs server )

```
 npm run dev
```


